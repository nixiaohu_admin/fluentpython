"""
元祖和记录:
元组其实是对数据的记录：元组中的每个元素都存放了记录中一个字段的数据，外加这个字段的位置。正是这个位置信息给数据赋予了意义。

如果只把元组理解为不可变的列表，那其他信息——它所含有的元素的总数和它们的位置——似乎就变得可有可无。但是如果把元组当作一些字段
的集合，那么数量和位置信息就变得非常重要了。
"""

# TODO: 1.洛杉矶国际机场的经纬度
# lax_coordinates = (33.9425, -118.408056)

# TODO: 2.东京市的一些信息：市名、年份、人口（单位：百万）、人口变化（单位：百分比）和面积（单位：平方千米）
# city, year, pop, chg, area = ('Tokyo', 2003, 32450, 0.66, 8014)

# TODO: 3.一个元组列表，元组的形式为(country_code, passport_number)
# traveler_ids = [('USA', '31195855'), ('BRA', 'CE342567'), ('ESP', 'XDA205856')]
# travelersorted_ids = sorted(traveler_ids)
# for passport in travelersorted_ids:
#     """
#     在迭代的过程中，passport变量被绑定到每个元组上
#     %格式运算符能被匹配到对应的元组元素上
#     BRA/CE342567
#     ESP/XDA205856
#     USA/31195855
#     """
#     print('%s/%s' % passport)
# print('-' * 150)

# for country, _ in travelersorted_ids:
#     """
#      for循环可以分别提取元组里的元素，也叫作拆包（unpacking）。
#      因为元组中第二个元素对我们没有什么用，所以它赋值给“_”占位符。
#     BRA
#     ESP
#     USA
#     """
#     print(country)
# print('-' * 150)
#
# print(country)
# print(type(country))
# print('-' * 150)

# TODO: 不使用中间变量交换两个变量
# a = 20
# b = 10
# b, a = a, b
# print(a)  # TODO: 10
# print(b)  # TODO: 20
# print('-' * 150)

"""
下面是另一个例子，这里元组拆包的用法则是让一个函数可以用元组的形式返回多个值，然后调用函数的代码就能轻松地接受这些返回值。
比如os.path.split() 函数就会返回以路径和最后一个文件名组成的元组 (path, last_part)
"""
import os

# file_path_list = os.path.split('/home/luciano/.ssh/idrsa.pub')
# path, last_part = file_path_list
# print('文件路径：', path)
# print('文件后缀及文件名：', last_part)
# print('-' * 150)

"""
用*来处理剩下的元素
在Python中，函数用*args来获取不确定数量的参数算是一种经典写法了。
"""
# a, b, *rest = range(5)
# result = (a, b, rest)
# print(result)   # TODO: (0, 1, [2, 3, 4])
# print('-' * 150)


# TODO: 嵌套元组拆包
"""
接受表达式的元组可以是嵌套式的，例如(a, b, (c, d))。
只要这个接受元组的嵌套结构符合表达式本身的嵌套结构，Python 就可以作出正确的对应。
"""
# metro_areas = [
#     ('Tokyo', 'JP', 36.933, (35.689722, 139.691667)),
#     ('Delhi NCR', 'IN', 21.935, (28.613889, 77.208889)),
#     ('Mexico City', 'MX', 20.142, (19.433333, -99.133333)),
#     ('New York-Newark', 'US', 20.104, (40.808611, -74.020386)),
#     ('Sao Paulo', 'BR', 19.649, (-23.547778, -46.635833))
# ]
# for name, cc, pop, (latitude, longitude) in metro_areas:
#     if longitude <= 0:
#         print('%s | %s | %s' % (name, latitude, longitude))


# TODO: 4.具名元祖
"""
collections.namedtuple是一个工厂函数，它可以用来构建一个带字段名的元组和一个有名字的类

用namedtuple构建的类的实例所消耗的内存跟元组是一样的，因为字段名都被存在对应的类里面。
这个实例跟普通的对象实例比起来也要小一些，因为Python 不会用__dict__来存放这些实例的属性。
"""
from collections import namedtuple

City = namedtuple('City', field_names=['name', 'country', 'population', 'coordinates'])
tokyo = City('Tokyo', 'JP', 36.933, (35.689722, 139.691667))
print(tokyo)
print(tokyo.population)
print(tokyo.coordinates)
print('*' * 50)

# _fields: 属性是一个包含这个类所有字段名称的元组。
print(City._fields)  # TODO: ('name', 'country', 'population', 'coordinates')

# 用_make(): 通过接受一个可迭代对象来生成这个类的一个实例，它的作用跟City(*delhi_data)是一样的。
LatLong = namedtuple('LatLong', field_names=['lat', 'long'])
delhi_data = ('Delhi NCR', 'IN', 21.935, LatLong(28.613889, 77.208889))
delhi = City._make(delhi_data)
print(delhi)

# _asdict(): 把具名元组以 collections.OrderedDict的形式返回，我们可以利用它来把元组里的信息友好地呈现出来。
# TODO: {'name': 'Delhi NCR', 'country': 'IN',
#  'population': 21.935, 'coordinates': LatLong(lat=28.613889, long=77.208889)}
print(delhi._asdict())
for key, value in delhi._asdict().items():
    print('%s: %s' % (key, value))
