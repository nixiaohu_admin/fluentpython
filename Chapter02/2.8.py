"""
TODO: 用bisect来管理已排序的序列
bisect模块包含两个主要函数，bisect(查找)和insort(插入)，两个函数都利用二分查找算法来在有序序列中查找或插入元素
"""

# TODO: 1.用bisect来搜索
import bisect
import sys
import time

HAYSTACK = [1, 4, 5, 6, 8, 12, 15, 20, 21, 23, 23, 26, 29, 30]
NEEDLES = [0, 1, 2, 5, 8, 10, 22, 23, 29, 30, 31]
ROW_FMT = '{0:2d} @ {1:2d} {2}{0:<2d}'


def demo(bisectfn):
    print('HAYSTACK len ->', len(HAYSTACK))
    for needle in reversed(NEEDLES):
        position = bisectfn(HAYSTACK, needle)  # TODO: 1.用特定的bisect函数来计算元素应该出现的位置
        offset = position * '|'  # TODO: 2.利用该位置来算出需要几个分隔符号
        print(ROW_FMT.format(needle, position, offset))  # TODO: 3.把元素和其应该出现的位置打印出来


if __name__ == '__main__':
    t1 = time.time()
    if sys.argv[-1] == 'left':  # TODO: 4.根据命令上最后一个参数来选用bisect函数
        bisect_fn = bisect.bisect_left
    else:
        bisect_fn = bisect.bisect
    print('DEMO：', bisect_fn.__name__)  # TODO: 5.把选定的函数在抬头打印出来
    print('haystack ->', ' '.join('%2d' % n for n in HAYSTACK))
    demo(bisect_fn)
    t2 = time.time()
    diff = t2 - t1
    print('运行时间消耗 ->', diff)
print('-' * 100)

"""
bisect.bisect_left(breakpoints, score)

分数 = 33, 成绩节点下标 = 0, 等级 = 不及格
分数 = 59, 成绩节点下标 = 0, 等级 = 不及格
分数 = 99, 成绩节点下标 = 4, 等级 = 顶尖
分数 = 77, 成绩节点下标 = 2, 等级 = 良好
分数 = 70, 成绩节点下标 = 1, 等级 = 及格
分数 = 89, 成绩节点下标 = 3, 等级 = 优秀
分数 = 90, 成绩节点下标 = 3, 等级 = 优秀
分数 = 100, 成绩节点下标 = 4, 等级 = 顶尖
"""

"""
bisect.bisect(breakpoints, score)
分数 = 33, 成绩节点下标 = 0, 等级 = 不及格
分数 = 59, 成绩节点下标 = 0, 等级 = 不及格
分数 = 99, 成绩节点下标 = 4, 等级 = 顶尖
分数 = 77, 成绩节点下标 = 2, 等级 = 良好
分数 = 70, 成绩节点下标 = 2, 等级 = 良好
分数 = 89, 成绩节点下标 = 3, 等级 = 优秀
分数 = 90, 成绩节点下标 = 4, 等级 = 顶尖
分数 = 100, 成绩节点下标 = 4, 等级 = 顶尖
"""


# TODO: 2.根据一个分数，找到它所对应的成绩
def grade(score, breakpoints=[60, 70, 80, 90], grades=['不及格', '及格', '良好', '优秀', '顶尖']):
    position = bisect.bisect(breakpoints, score)
    # position = bisect.bisect_left(breakpoints, score)
    result = grades[position]
    print('分数 = %s, 成绩节点下标 = %s, 等级 = %s' % (score, position, result))
    return result


[grade(score) for score in [33, 59, 99, 77, 70, 89, 90, 100]]
print('-' * 100)

# TODO: 3.用bisect.insort插入新元素(并能保持排序)
# insort(seq, item)把变量 item插入到序列 seq中，并能保持 seq的升序顺序

import random

s = time.time()
SIZE = 7
random.seed(1729)
my_list = []
for i in range(SIZE):
    new_item = random.randrange(SIZE * 2)
    bisect.insort(my_list, new_item)
    print('%2d ->' % new_item, my_list)
e = time.time()
print('运行时间消耗 ->', e - s)
