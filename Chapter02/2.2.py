"""
列表推导表达式
列表推导的作用：生成列表
"""

# TODO: 1.把一个字符串变成 Unicode 码位的列表

# symbols = '$¢£¥€¤'
# code = 'old code'
# codes = [ord(code) for code in symbols]
# print(code)
# print(codes)    # TODO: [36, 162, 163, 165, 8364, 164]


# TODO: 2.列表推导同filter和map的比较

# symbols = '$¢£¥€¤'
# codes = [ord(code) for code in symbols if ord(code) > 127]
# print(codes)
#
# filter_codes = list(filter(lambda code: code > 127, map(ord, symbols)))
# print(filter_codes)  # TODO: [162, 163, 165, 8364, 164]
# print(list(map(ord, symbols)))  # TODO: [36, 162, 163, 165, 8364, 164]


# TODO: 3.列表推导计算笛卡尔积
# TODO: 如果你需要一个列表，列表里是3种不同尺寸的T恤衫，每个尺寸都有2个颜色
# colors = ['白色', '黑色']
# sizes = ['小号', '中号', '大号']
# t_shirts = [(color, size) for color in colors for size in sizes]  # TODO: T恤
# print(t_shirts)
# print(len(t_shirts))


"""
生成器表达式
生成器表达式背后遵守了迭代器协议，可以逐个地产出元素，而不是先建立一个完整的列表，然后再把这个列表传递到某个构造函数里。
前面那种方式显然能够节省内存。生成器表达式的语法跟列表推导差不多，只不过把方括号换成圆括号而已。
"""
symbols = '$¢£¥€¤'
codes = tuple((ord(code) for code in symbols))
print(codes)  # TODO: (36, 162, 163, 165, 8364, 164)
print('*' * 50)

import array

symbols_array = array.array('I', (ord(code) for code in symbols))
print(symbols_array)  # TODO: array('I', [36, 162, 163, 165, 8364, 164])
print('*' * 50)

# TODO: 使用生成器计算笛卡尔积
"""
生成器表达式逐个产出元素，从来不会一次性产出一个含有6个T恤样式的列表
"""
colors = ['白色', '黑色']
sizes = ['小号', '中号', '大号']
t_shirts = tuple(((color, size) for color in colors for size in sizes))
print(t_shirts)
