"""
TODO: 当列表不是首选时
"""

# TODO: 1.数组
"""
一个浮点型数组的创建、存入文件和从文件读取的过程
"""

from array import array  # TODO: 1.引入 array类型
from random import random
import time

# t1 = time.time()
# # TODO 2.利用一个可迭代对象来建立一个双精度浮点数组（类型码是'd'），这里我们用的可迭代对象是一个生成器表达式。
# floats = array('d', (random() for i in range(10 ** 7)))
# print(len(floats))
# print(floats[-1])   # TODO 3.查看数组的最后一个元素
# # TODO: 存入文件
# fp = open('./floats.bin', 'wb')
# floats.tofile(fp)   # TODO 4.把数组存入一个二进制文件里
# fp.close()
#
# # TODO: 读取文件
# floats2 = array('d')    # TODO 5.新建一个双精度浮点空数组。
# fp = open('./floats.bin', 'rb')
# floats2.fromfile(fp, 10**7)     # TODO 6.把 1000 万个浮点数从二进制文件里读取出来
# fp.close()
#
# print(len(floats2))
# print(floats2[-1])      # TODO 7. 查看新数组的最后一个元素
# print(floats2 == floats)    # TODO 8.检查两个数组的内容是不是完全一样
# t2 = time.time()
# diff = t2 - t1
# print('运行时间消耗 ->', diff)
# print('-' * 100)


# TODO: 2.内存视图
"""
通过改变数组中的一个字节来更新数组里某个元素的值
"""
numbers = array('h', [-2, -1, 0, 1, 2])
memv = memoryview(numbers)
print(len(memv))
print(memv[0])
