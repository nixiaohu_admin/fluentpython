"""
TODO: 1 为什么切片和区间会忽略最后一个元素
●当只有最后一个位置信息时，我们也可以快速看出切片和区间里有几个元素：range(3)和my_list[:3]都返回 3个元素。
●当起止位置信息都可见时，我们可以快速计算出切片和区间的长度，用后一个数减去第一个下标（stop - start）即可。
●这样做也让我们可以利用任意一个下标来把序列分割成不重叠的两部分，只要写成my_list[:x]和my_list[x:]就可以了，如下所示。
"""

# TODO: 在下标3的地方进行分割
l = [10, 20, 30, 40, 50, 60]
print(l[:3])
print(l[3:])
print('-' * 100)

"""
TODO: 2.对对象进行切片
一个众所周知的秘密是，我们还可以用s[a:b:c]的形式对s，在a和b之间以c为间隔取值。c的值还可以为负，负值意味着反向取值
"""
invoice = """
... 
0.....6................................40........52...55........
...$17.50    3    $52.50
...$4.95     2    $9.90
...$28.00    1    $28.00
...$34.95    1    $34.95... 
"""
line_items = invoice.split('\n')[3:-1]
print(line_items)
goods_price = slice(4, 8)
goods_count = slice(13, 17)
for item in line_items:
    """
    17.5 | 3   
    4.95 | 2   
    28.0 | 1   
    34.9 | 1  
    """
    print('%s | %s' % (item[goods_price], item[goods_count]))
print('-' * 100)

"""
TODO: 3.给切片赋值
如果把切片放在赋值语句的左边，或把它作为del操作的对象，我们就可以对序列进行嫁接、切除或就地修改操作。
"""
num_list = list(range(11))
print(num_list)
num_list[2:5] = [20, 30]
print(num_list)
del num_list[5:7]
print(num_list)
num_list[3:6:2] = [11, 22]
print(num_list)
num_list[2:5] = [100]
print(num_list)
