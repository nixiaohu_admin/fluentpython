"""
TODO: 1.序列的增量赋值
一个关于+=的谜题
"""

"""
一个谜题
t = (1, 2, [30, 40])
t[2] += [50, 60]
到底会发生下面 4种情况中的哪一种？
a. t变成 (1, 2, [30, 40, 50, 60])。
b. 因为tuple不支持对它的元素赋值，所以会抛出 TypeError异常。
c. 以上两个都不是。
d. a和b都是对的。

没人料到的结果：t[2]被改动了，但是也有异常抛出
正确答案： D
"""

t2 = (1, 2, [30, 40])
try:
    t2[2] += [50, 60]
except Exception as error:
    print(error)    # TODO: 抛出异常：'tuple' object does not support item assignment
print(t2)   # TODO: 更改成功： (1, 2, [30, 40, 50, 60])
