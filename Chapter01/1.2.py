from math import hypot


class Vector(object):
    """
    二维向量加、乘法
    用这些特殊方法实现的：__repr__、__abs__、__add__和__mul__
    """

    def __init__(self, x=0, y=0):
        self.x = x
        self.y = y

    def __abs__(self):
        return hypot(self.x, self.y)

    # TODO: 定义加法
    """
    返回新创建的对象，不改变原对象
    """
    def __add__(self, other):
        print('调用__add__')
        return Vector(self.x + other.x, self.y + other.y)

    # TODO: 定义乘法
    """
    返回新创建的对象，不改变原对象
    """
    def __mul__(self, other):
        print('调用__mul__')
        return Vector(self.x * other.x, self.y * other.y)

    # TODO: 自定义布尔值
    """
    0 返回 False 
    非0 返回 True
    """
    def __bool__(self):
        print('调用__bool__')
        return bool(abs(self.x) or abs(self.y))

    # TODO: 把一个对象用字符串的形式表达出来
    def __repr__(self):
        print('调用__repr__')
        return '<Vector(%s, %s)>' % (self.x, self.y)


v1 = Vector(2, 2)
v2 = Vector(3, 3)
print(v1 + v2)
print(v1 * v2)
print('*' * 30)
print(v1)
print(v2)
print('*' * 30)
print(bool(v1))
print(bool(v2))

