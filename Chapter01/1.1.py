"""
TODO: 一摞有序的纸牌
"""
import collections
import random

# TODO: 1.用collections.namedtuple构建了一个简单的类来表示一张纸牌
Card = collections.namedtuple('Card', ['rank', 'suit'])


class FrenchDeck(object):
    """
    创建纸牌类
    """
    ranks = [str(n) for n in range(2, 11)] + list('JQKA')  # TODO: 生成纸牌点数 2-10 J、Q、K、A
    suits = '黑桃 方块 梅花 红心'.split(" ")

    def __init__(self):
        self._cards = [Card(rank, suit) for suit in self.suits for rank in self.ranks]

    def __len__(self):
        print('len(obj)底层调用...')
        return len(self._cards)

    def __getitem__(self, position):
        print('obj[key]底层调用...')
        return self._cards[position]


# TODO: namedtuple构建类
beer_card = Card('7', '方块')
print(beer_card)  # TODO: Card(rank='7', suit='方块')
# print(beer_card.__dir__())
print('*' * 30)

# TODO: 访问所以纸牌
deck = FrenchDeck()
print(len(deck))

# TODO: 通过下标随机抽取纸牌
print(deck[0])
print(deck[-1])
print('*' * 30)

# TODO: 随机抽取纸牌
print(random.choice(deck))
print(random.choice(deck))
print(random.choice(deck))
print('*' * 30)

# TODO: 切片抽取
print(deck[0:3])  # TODO: 抽取前3张扑克
print(deck[12::13])  # TODO: （抽取A的所有扑克）从下标12处，每隔13张抽取
print('*' * 30)

# TODO: in 运算符
"""
一个集合类型没有实现 __contains__方法，那么 in运算符就会按顺序做一次迭代搜索
"""
print(Card(rank='2', suit='梅花') in deck)  # TODO: True
print(Card(rank='12', suit='梅花') in deck)  # TODO: False
print('*' * 30)

# TODO: 扑克排序
"""
规则: 
黑桃 > 红心 > 方块 > 梅花 
用点数来判定扑克牌的大小，2 最小、A 最大；
同时还要加上对花色的判定，黑桃最大、红桃次之、方块再次、梅花最小。
下面就是按照这个规则来给扑克牌排序的函数: 
梅花 2的大小是0  黑桃 A是51
"""
suit_values = {  # TODO: 声明花色大小
    '黑桃': 3,
    '红心': 2,
    '方块': 1,
    '梅花': 0
}


def spades_high(card_item):
    """
    计算扑克点数
    计算公式：获取获取扑克点数下标 * 花色数量 + 花色对应的value
    :param card_item:
    :return:
    """
    rank_value = FrenchDeck.ranks.index(card_item.rank)  # TODO: 获取扑克点数下标
    return rank_value * len(suit_values) + suit_values[card_item.suit]


# TODO: 逆序排序 -> reversed(sorted(deck, key=spades_high))
for card in sorted(deck, key=spades_high):
    print(card)

print('*' * 30)
