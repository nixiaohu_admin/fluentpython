# fluentpython

#### 介绍
流畅的Python 丛书代码

#### 软件架构
软件架构说明


#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)

* Chapter01 `Python 数据模型`
    * 1.1.py `一摞Python风格的纸牌`
    * 1.2.py `二维向量加法 类(经度, 纬度)`
* Chapter02 `数据结构`
    * 2.2.py `列表推导和生成器表达式`
    * 2.3.py `元组不仅仅是不可变的列表`
        * 元祖和记录
        * 元祖拆包
        * 嵌套元祖拆包
        * 具名元祖
    * 2.4.py `切片`
        * 为什么切片和区间会忽略最后一个元素
        * 对对象进行切片
        * 给切片赋值
    * 2.5.py `对序列使用+和*`
        * 建立由列表组成的列表
    * 2.6.py `序列的增量赋值`
        * 一个关于+=的谜题
    * 2.8.py `用bisect来管理已排序的序列`
        * 用bisect来搜索
        * 根据一个分数，找到它所对应的成绩
        * 用bisect.insort插入新元素(并能保持排序)
    * 2.9.py `当列表不是首选时`
        * 数组
            * 从创建一个有1000 万个随机浮点数的数组开始，到如何把这个数组存放到文件里，再到如何从文件读取这个数组
        * 内存视图
            * 通过改变数组中的一个字节来更新数组里某个元素的值
